<h1>Material Layers</h1>
<h4>Description</h4>
Material Layers is an attempt to simplify building layered websites based on material design principles. 

<h4>Features</h4>

*  Automatically generate shadows based on layers
*  Automatically generate height animations based on layer distance - In Progress