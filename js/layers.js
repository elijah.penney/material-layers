(function () {
  "use strict";

  let layerCheck = new RegExp(/(layer)\-\d{1,2}/);
  let hoverCheck = new RegExp(/(hover)\-\d{1,2}/);

  /**
   * Checks if the body tag is defined as layer-0, if it is not, add it.
   * @author Elijah Penney
   */

  if (document.getElementsByTagName("body")[0].className.indexOf("layer-0") == -1) {
    document.getElementsByTagName("body")[0].classList.add("layer-0");
  }

  /**
   * Takes an HTML element as input and returns it"s layer number if it has one
   * @author Elijah Penney
   * @param   {object} element - An HTML Node / Element Object
   * @returns {string} - Returns unparsed layer number
   */
  function retrieveLayerNumber(element) {

    let elementClassList = Array.prototype.slice.call(element.classList);

    if (-1 !== element.className.indexOf("layer-")) {
      let layerNumber = elementClassList.filter(function (elementClass) {
        if (layerCheck.exec(elementClass)) return elementClass;
      })[0].split("layer-")[1];

      return layerNumber;

    }
  }

  /**
   * Takes an HTML element and searches through parent nodes till it finds the next defined layer
   * @author Elijah Penney
   * @param   {object} element - An HTML Node / Element Object
   * @returns {string} - Returns unparsed layer number
   */
  function findParentLayer(element) {

    let parentClassList = element.parentNode.classList;

    if (0 !== parentClassList.length) {
      for (let i = 0; i <= parentClassList.length; i++)
        if (layerCheck.exec(parentClassList[i])) {
          return parseInt(parentClassList[i].split("layer-")[1]);
        }
    } else {
      return findParentLayer(element.parentNode);
    }
  }

  /**
   * Actually applies our stylings to our layers
   * @author Elijah Penney
   * @param {object} element            - Element we are applying the styling to
   * @param {number} layerNumber        - The layer at which this element is sitting on
   * @param {number} parentLayerNumber  - The layer at which this elements parent is sitting on 
   */
  function applyStyling(element, layerNumber, parentLayerNumber) {

    let restingLayerHeight;

    if (layerNumber !== parentLayerNumber) {
      restingLayerHeight = layerNumber;
    } else {
      restingLayerHeight = parentLayerNumber;
    }

    // Add our z-index to the elements
    element.style.zIndex = restingLayerHeight;

    // Check if this is an element that has a hover class
    if (hoverCheck.exec(element.className)) {
      addHoverTransition(element, layerNumber);

      // Add event to change z-index on mouseover event
      element.onmouseover = function () {
        element.style.zIndex = (getHoverLayer(element) - layerNumber) + restingLayerHeight;
      }

      // Add event to change z-index on mouseout event
      element.onmouseout = function () {
        element.style.zIndex = restingLayerHeight;
      }
    }

    // Check if this element has a depth
    if ((layerNumber - parentLayerNumber) !== 0) {
      element.classList.add("shadow-depth-" + (layerNumber - parentLayerNumber));
    }

  }

  /**
   * Dynamically determine the amount of time the box shadow should take to transition
   * @author Elijah Penney
   * @param {object} element     - Element we are applying the styling to
   * @param {number} layerNumber - The layer at which this element is sitting on
   */
  function addHoverTransition(element, layerNumber) {

    let layerDifference = Math.abs(Math.abs(getHoverLayer(element)) - layerNumber);

    if (layerDifference !== 0) {
      element.classList.add('transition-' + layerDifference);
    } else {
      console.log("Attempted to hover from " + layerNumber + " to layer " + layerNumber + ". This cannot be done.");
    }
  }

  /**
   * Retrieve the layer the element will be hovering to
   * @author Elijah Penney
   * @param   {object} element - Element we are applying the styling to
   * @returns {number}         - The layer at which we will be hovering to
   */
  function getHoverLayer(element) {

    let elementClassList = element.classList;

    if (-1 !== element.className.indexOf("hover-")) {
      for (let i = 0; i <= elementClassList.length; i++)
        if (hoverCheck.exec(elementClassList[i])) return parseInt(elementClassList[i].split("hover-")[1]);
    }

  }

  let elementArray = document.querySelector("body").querySelectorAll("*");

  for (let i = 0; i < elementArray.length; i++) {
    // Checks to make sure it is a valid node
    if ("SCRIPT" !== elementArray[i].nodeName) {
      // Gets the layer number of our current element
      let layerNumber = parseInt(retrieveLayerNumber(elementArray[i]));

      // If the element is not marked as a layer, assume the layer of it's closest layered parent
      if (isNaN(layerNumber)) {
        layerNumber = findParentLayer(elementArray[i]);
        if (isNaN(layerNumber)) {
          layerNumber = 0;
        }
      }

      // Gets the layer number of the next parent with a defined layer
      let parentLayerNumber = parseInt(findParentLayer(elementArray[i]));

      // If the parent layer returns as not a number, set it to 0
      // This is to fix issues with the body being recognized as layer-0 but not returning layer-0
      if (isNaN(parentLayerNumber)) {
        parentLayerNumber = 0;
      }
      applyStyling(elementArray[i], layerNumber, parentLayerNumber);
    }
  }

})();
